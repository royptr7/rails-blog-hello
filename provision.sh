#!/usr/bin/env bash

# Install system dependency
is_installed=`which ruby`
if [ -z $is_installed]; then
  sudo apt-add-repository ppa:brightbox/ruby-ng -y
  sudo apt-get update
  sudo apt-get install ruby2.4 ruby2.4-dev -y
  sudo apt-get install libsqlite3-dev -y
  sudo apt-get install nodejs -y
  sudo gem install bundle
fi

# Get source code
sudo apt-get install git -y
cd /opt
sudo git clone https://gitlab.com/royptr7/rails-blog-hello.git
sudo chown -R vagrant:vagrant rails-blog-hello/
cd rails-blog-hello
git pull
#chown -R vagrant:vagrant rails-blog-hello/

# Install app dependency
bundle install

# Run the app
sudo pkill -f "puma 3.11.3 (tcp://0.0.0.0:3000) [vagrant]"
./run.sh

